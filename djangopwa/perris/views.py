from django.shortcuts import render
from django.core import serializers
from . models import Dog
import json
# Create your views here.
def index(request):
	template='index.html'
	results=Dog.objects.all()
	context={
		'results':results,
	}
	return render(request,template,context)

def base_layout(request):
	template='base.html'
	return render(request,template)

def getdata(request):
    results=Dog.objects.all()
    jsondata = serializers.serialize('json',results)
    return HttpResponse(jsondata)