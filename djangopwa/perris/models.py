from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver

# Create your models here.
class Persona(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_admin = models.BooleanField('Administrador', default=False)
    can_adopt = models.BooleanField('Puede Adoptar',default=False)
    apellidom = models.CharField(max_length=40)
    telefono = models.CharField(max_length=40)
    rut = models.CharField(max_length=40)
    region =  models.CharField(max_length=40, null=True)
    comuna =  models.CharField(max_length=40, null=True)
    vivienda = models.IntegerField(blank=True, null=True)
    fnac = models.DateField(blank=True, null=True)
    foto = models.ImageField(upload_to="UserImages/")
    def __str__(self):
        return "PERSONA"

class Dog(models.Model):
    STATE_CHOICES = (('RE','rescatado'),('DI','disponible'),('AD','adoptado'))
    foto = models.ImageField(upload_to="DogsImages/")
    nombre = models.CharField(max_length=40)
    raza = models.CharField(max_length=40)
    descripcion = models.CharField(max_length=160)
    estado = models.CharField(max_length=2, choices=STATE_CHOICES, default='RE')
    autor = models.ForeignKey(Persona,on_delete=models.SET_NULL, null=True, related_name='autor')
    adopter = models.ForeignKey(Persona,on_delete=models.SET_NULL,null=True, related_name='adopter')
    def __str__(self):
        return "DOG"